package com.example.sadak.mytodolist.Models;

import android.app.Activity;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.TextView;

import com.example.sadak.mytodolist.HomeActivity;
import com.example.sadak.mytodolist.R;

import java.util.ArrayList;
import java.util.List;

public class DailyRoutineAdapter  extends ArrayAdapter<DailyRoutine> {

    private HomeActivity context;
    private List<DailyRoutine> routineList;
    private List<DailyRoutine> selectedRoutine = new ArrayList<>();

    public DailyRoutineAdapter(Activity context, List<DailyRoutine> DailyRoutineAdapter) {
        super(context, R.layout.todo_listview, DailyRoutineAdapter);
        this.context = (HomeActivity) context;
        this.routineList = DailyRoutineAdapter;
    }

    @NonNull
    @Override
    public View getView(final int position, @Nullable View convertView, @NonNull ViewGroup parent) {

        LayoutInflater inflater = context.getLayoutInflater();

        View listViewItem = convertView;
        if (listViewItem == null) listViewItem = inflater.inflate(R.layout.todo_listview, null, true);

        TextView workDescription = listViewItem.findViewById(R.id.tvdescription);
        TextView wishDate = listViewItem.findViewById(R.id.tvDate);
        CheckBox cb = listViewItem.findViewById(R.id.checkBox3);

        cb.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    selectedRoutine.add(routineList.get(position));
                }else{
                    selectedRoutine.remove(routineList.get(position));
                }

                if (selectedRoutine.size() > 0) {
                    context.showOptions();
                }else{
                    context.hideOptions();
                }
            }
        });

        DailyRoutine dailyRoutine = routineList.get(position);

        workDescription.setText(dailyRoutine.getDescription());
        wishDate.setText(dailyRoutine.getWishDate());

        return listViewItem;
    }

    public List<DailyRoutine> getSelectedRoutine() {
        return selectedRoutine;
    }
}