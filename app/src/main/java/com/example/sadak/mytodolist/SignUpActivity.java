package com.example.sadak.mytodolist;

import android.app.ProgressDialog;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.example.sadak.mytodolist.Models.AppUser;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

public class SignUpActivity extends AppCompatActivity implements View.OnClickListener {

    EditText etName,etEmail,etPassword;
    Button btnRegister;
    AppUser appUser = new AppUser();

    private ProgressDialog progressDialog;
    private FirebaseAuth firebaseAuth;
    FirebaseDatabase fdb;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sign_up);

        etName = findViewById(R.id.nameEt);
        etEmail = findViewById(R.id.emailEt);
        etPassword = findViewById(R.id.passwordEt);
        progressDialog = new ProgressDialog(this);
        btnRegister = findViewById(R.id.registerbt);
        firebaseAuth = FirebaseAuth.getInstance();
        fdb = FirebaseDatabase.getInstance();

        btnRegister.setOnClickListener(this);




    }
    private void registerUser(){

        String Email = etEmail.getText().toString().trim();
        String Password = etPassword.getText().toString().trim();

        if (TextUtils.isEmpty(Email)) {
            Toast.makeText(SignUpActivity.this, "Enter the Email", Toast.LENGTH_SHORT).show();
            return;
        }
        if (TextUtils.isEmpty(Password)) {
            Toast.makeText(SignUpActivity.this, "Enter the Pasword", Toast.LENGTH_SHORT).show();
            return;
        }
        progressDialog.setMessage("please wait a while..");
        progressDialog.show();
        firebaseAuth.createUserWithEmailAndPassword(Email, Password)
                .addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        if (task.isSuccessful()) {
                            storeUserData();
                        } else {
                            Log.e("RegistrationException", "NOT registered " + task.getException());
                            Toast.makeText(SignUpActivity.this, task.getException().getMessage(), Toast.LENGTH_SHORT).show();
                            progressDialog.dismiss();
                        }
                    }
                });


    }
    private void storeUserData() {
        appUser.setUserEmail(etEmail.getText().toString().trim());
        appUser.setUserName(etName.getText().toString().trim());
        appUser.setUserPassword(etPassword.getText().toString().trim());

        DatabaseReference userRef = fdb.getReference("AppUsers");
        userRef.child(appUser.getUserEmail().replace("@", "-").replace(".", "-")).setValue(appUser);




        progressDialog.dismiss();
        Toast.makeText(SignUpActivity.this, "Successfully registered", Toast.LENGTH_SHORT).show();
        finish();
        startActivity(new Intent(getApplicationContext(), HomeActivity.class));
    }


    @Override
    public void onClick(View v) {
        if(v == btnRegister)
        {

            registerUser();
        }
    }
}
