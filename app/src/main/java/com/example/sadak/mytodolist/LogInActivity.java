package com.example.sadak.mytodolist;

import android.app.ProgressDialog;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;

public class LogInActivity extends AppCompatActivity implements View.OnClickListener {

    EditText etEmail,etPassword;
    Button btnLogIn, btnSignUp;
    ProgressDialog progressDialog;
    FirebaseAuth firebaseAuth;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_log_in);

        etEmail = findViewById(R.id.etEmail);
        etPassword = findViewById(R.id.etPassword);

        btnLogIn = findViewById(R.id.btLogIn);
        btnSignUp = findViewById(R.id.btSignUp);

        firebaseAuth =firebaseAuth.getInstance();
        if(firebaseAuth.getCurrentUser() != null){

            finish();
            startActivity(new Intent(getApplicationContext(),HomeActivity.class));
        }
        progressDialog = new ProgressDialog(this);

        btnLogIn.setOnClickListener(this);
        btnSignUp.setOnClickListener(this);
    }
    private void userLogin(){

        String Email = etEmail.getText().toString().trim();
        String Password = etPassword.getText().toString().trim();

        if (TextUtils.isEmpty(Email)) {
            Toast.makeText(LogInActivity.this, "Enter the Email", Toast.LENGTH_SHORT).show();
            return;
        }
        if (TextUtils.isEmpty(Password)) {
            Toast.makeText(LogInActivity.this, "Enter the Pasword", Toast.LENGTH_SHORT).show();
            return;
        }
        progressDialog.setMessage("please wait a while..");
        progressDialog.show();
        firebaseAuth.signInWithEmailAndPassword(Email,Password)
                .addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        progressDialog.dismiss();
                        if(task.isSuccessful()){
                            finish();
                            startActivity(new Intent(getApplicationContext(),HomeActivity.class));
                        }
                        else{
                            Toast.makeText(LogInActivity.this, task.getException().getMessage(), Toast.LENGTH_SHORT).show();
                            return;
                        }
                    }
                });

    }

    @Override
    public void onClick(View v) {

        if(v == btnLogIn)
        {
            userLogin();
        }

        else if (v == btnSignUp)
        {

            Intent intent= new Intent(LogInActivity.this,SignUpActivity.class);
            startActivity(intent);
        }

    }
}
