package com.example.sadak.mytodolist;

import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.webkit.WebView;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.Toast;

import com.example.sadak.mytodolist.Models.DailyRoutine;
import com.example.sadak.mytodolist.Models.DailyRoutineAdapter;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import java.util.ArrayList;
import java.util.List;

public class HomeActivity extends AppCompatActivity {

    LinearLayout llOptionContainer;

    ImageView delete,complete;
    ImageView addActivities, seeMore, logOut;
    Button btnDelete,btnComplete;

    ListView lvActivities;
    //Button btnLogOut;
    private FirebaseAuth firebaseAuth;
    FirebaseUser user;
    List<DailyRoutine> routineList;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);

        llOptionContainer = findViewById(R.id.options);
        btnDelete = findViewById(R.id.btnDelete);
       // btnComplete = findViewById(R.id.btnDelete);

        addActivities = findViewById(R.id.imAdd);
        lvActivities = findViewById(R.id.lvToDo);
        //btnLogOut = findViewById(R.id.logOut);
        logOut = findViewById(R.id.imSee);

        firebaseAuth = FirebaseAuth.getInstance();
        routineList = new ArrayList<>();
        seeMore = findViewById(R.id.imSee);

        addActivities.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(HomeActivity.this, AddListActivity.class));

            }
        });

        user = firebaseAuth.getCurrentUser();
        final String userId = user.getEmail().replace("@", "-").replace(".", "-");

        FirebaseDatabase fdb = FirebaseDatabase.getInstance();
        final DatabaseReference ref = fdb.getReference("WorkList").child(userId);

        ref.addChildEventListener(new ChildEventListener() {
            @Override
            public void onChildAdded(@NonNull DataSnapshot dataSnapshot, @Nullable String s) {
                DailyRoutine dailyRoutine = dataSnapshot.getValue(DailyRoutine.class);
                routineList.add(dailyRoutine);
                final DailyRoutineAdapter adapter = new DailyRoutineAdapter(HomeActivity.this, routineList);
                lvActivities.setAdapter(adapter);

                btnDelete.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        List<DailyRoutine> routines = adapter.getSelectedRoutine();
                        if (routines != null)
                            for (int i = 0; i < routines.size(); i++) {
                                Toast.makeText(HomeActivity.this, "Delete: " + routines.get(i).getDescription(), Toast.LENGTH_SHORT).show();
                                Log.e("DEBUG","Delete: " + routines.get(i).getDescription());


                            }

                    }
                });
            }

            @Override
            public void onChildChanged(@NonNull DataSnapshot dataSnapshot, @Nullable String s) {

            }

            @Override
            public void onChildRemoved(@NonNull DataSnapshot dataSnapshot) {

            }

            @Override
            public void onChildMoved(@NonNull DataSnapshot dataSnapshot, @Nullable String s) {

            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });

        logOut.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                firebaseAuth.signOut();
                finish();
                startActivity(new Intent(getApplicationContext(), LogInActivity.class));
            }
        });

    }

    public void showOptions() {
        llOptionContainer.setVisibility(View.VISIBLE);
    }

    public void hideOptions() {
        llOptionContainer.setVisibility(View.GONE);
    }
}
