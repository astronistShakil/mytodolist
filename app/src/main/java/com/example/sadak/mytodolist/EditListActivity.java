package com.example.sadak.mytodolist;

import android.app.DatePickerDialog;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.sadak.mytodolist.Models.AppUser;
import com.example.sadak.mytodolist.Models.DailyRoutine;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

public class EditListActivity extends AppCompatActivity {
    private static final String TAG = "HomeActivity";
    private DatePickerDialog.OnDateSetListener imageDate;

    ImageView addDate;
    TextView tvDate;
    Button btnSave;
    String dateSelect;
    EditText etToDoActivities;
    List<DailyRoutine> routineList;
    AppUser appUser;
    FirebaseUser user;
    FirebaseAuth firebaseAuth;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_list);

        addDate = findViewById(R.id.imDate);
        tvDate = findViewById(R.id.tvDate);
        btnSave = findViewById(R.id.btSave);
        etToDoActivities = findViewById(R.id.etDescription);
        routineList = new ArrayList<>();
        appUser = new AppUser();

        addDate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Calendar cal = Calendar.getInstance();
                int year = cal.get(Calendar.YEAR);
                int month = cal.get(Calendar.MONTH);
                int day = cal.get(Calendar.DAY_OF_MONTH);

                DatePickerDialog dialog = new DatePickerDialog(EditListActivity.this,
                        android.R.style.Theme_DeviceDefault,imageDate,year,month,day);
                dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.BLACK));
                dialog.show();
            }
        });

        imageDate = new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker datePicker, int i, int i1, int i2) {
                Log.d(TAG,"onDateSet : etDate : "+i + "/" + (i1+1) +"/" + i2);
                dateSelect = i+ "/" + (i1+1) + "/" + i2;
                tvDate.setText(dateSelect);
            }
        };



        btnSave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

            }
        });
    }
}
