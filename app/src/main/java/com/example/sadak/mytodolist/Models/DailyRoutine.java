package com.example.sadak.mytodolist.Models;

public class DailyRoutine {

    String wishDate,description;

   public DailyRoutine(){

   }

    public DailyRoutine(String wishDate, String description) {
        this.wishDate = wishDate;
        this.description = description;
    }

    public String getWishDate() {
        return wishDate;
    }

    public String getDescription() {
        return description;
    }
}
