package com.example.sadak.mytodolist.Models;

import com.google.firebase.database.DatabaseReference;

public class AppUser {

    private String userName;
    private String userEmail;
    private String userPassword;
    DatabaseReference ref;
    public AppUser(){

    }

    public DatabaseReference getRef() {
        return ref;
    }
    public void setRef(DatabaseReference ref) {
        this.ref = ref;
    }

    public AppUser(String userName, String userEmail, String userPassword) {
        this.userName = userName;
        this.userEmail = userEmail;
        this.userPassword = userPassword;
    }



    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getUserEmail() {
        return userEmail;
    }

    public void setUserEmail(String userEmail) {
        this.userEmail = userEmail;
    }

    public String getUserPassword() {
        return userPassword;
    }

    public void setUserPassword(String userPassword) {
        this.userPassword = userPassword;
    }

    @Override
    public String toString() {
        return "AppUser{" +
                "userName='" + userName + '\'' +
                ", userEmail='" + userEmail + '\'' +
                ", userPassword='" + userPassword + '\'' +
                '}';
    }
}
